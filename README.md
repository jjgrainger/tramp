# Tramp - LAMP Development Environment

> Local development server for PHP projects

## Requirements

* [Ansible](https://www.ansible.com/)
* [Vagrant](https://www.vagrantup.com/)
* [Virtualbox](https://www.virtualbox.org/)

All can be installed easily with [Homebrew](https://brew.sh/).

```shell
$ brew install ansible
$ brew cask install vagrant virtualbox
```

## Installation

To install simply clone the repository into your home directory.

```shell
$ git clone git@bitbucket.org:venncreative/tramp.git ~/tramp
```

## Setup

All settings and sites are configured in the `Sites.yaml` file.

##### Folders

The `folders:` array offers the ability to map what folders on the host you wish to mount onto the virtual machine.

As an example, we can map a folder on the host `~/localhost` to the virtual machines `/var/www` with the following:

```yaml
folders:
    - map: "~/localhost"
      to: "/var/www"
```

##### Sites

You can configure what sites you want with the `sites:` array.

Sites are defined by mapping the desired domain to a specific folder on the virtual machine. For example, we can map `example.dev` to `/var/www/example/public` with the following configuration:

```yaml
sites:
    - map: "example.dev"
      to: "/var/www/example/public"
```

Once you've created a site, you will need to update the host machines `hosts` file adding the desired domain and virtual machines ip address.

You can do this with your favourite text editor like so:

```shell
# Open the /etc/hosts file with sublime text
subl /etc/hosts
```

Inside the `hosts` file add the following on a new line:

```
192.168.33.50 example.dev
```

Save (and enter your password) to finish.

##### Start the server

With sites configured we can now boot up the server.

Inside the `~/tramp` folder, run `vagrant up`. This will setup the virtual machine and provision the server. This may take a while if it is the first time running.

First, you'll see Vagrant working to setup the initial box, after, ansible will kick in to provision the server.

## Usage

You will want to add additional sites as time goes on, this is done with 3 simple steps:

1. add another site to the `Sites.yaml` file
2. update the `/etc/hosts` file
3. re-provisioning the server

##### 1. Add another site the `Sites.yaml` file

Let's add venncreative.dev as a site. There should be a folder setup on the host machine at `~/localhost/venncreative`. Then we can add the domain and map it to that folder.

```yaml
sites:
    - map: "example.dev"
      to: "/var/www/example/public"
    - map: "venncreative.dev"
      to: "/var/www/venncreative"
```

##### 2. Update the `/etc/hosts` file

In the terminal run the following command to edit the hosts file in your favourite editor

```shell
# Open the /etc/hosts file with sublime text
subl /etc/hosts
```

Inside the `hosts` file add the venncreative.dev site on a new line:

```
192.168.33.50 venncreative.dev
```

##### 3. Re-provision the server

Inside the `~/tramp` folder, we can start up the server and re-provision it with the following command.

```shell
$ vagrant up --provision
```

If the virtual machine is already up and running, you can just re-provision the server with:

```shell
$ vagrant provision
```

Once the ansible has finished provisioning the server you should be able to visit the site at [http://venncreative.dev](http://venncreative.dev)


